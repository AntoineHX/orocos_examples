require "rttlib"
-- require "rttros"
require "os"

-- ------------------------------------------ --
-- Get the deployer and the necessary imports --
-- ------------------------------------------ --

-- get the deployer
tc=rtt.getTC()
if tc:getName() == "lua" then
  dep=tc:getPeer("Deployer")     
elseif tc:getName() == "Deployer" then
  dep=tc
end

-- do the necessary imports
dep:import("ocl")
dep:import("rtt_ros2_std_msgs")
dep:import("rtt_ros2")
dep:import("rtt_ros2_node")
rtt.provides("ros"):import("orocos_examples")

-- ------------------------- --
-- Important global settings --
-- ------------------------- --
ros_application_name = "SimpleRos"
app_period = 0.1
connect_to_topics = true

-- connect_to_topics = rtt.Variable("rclcpp.ParameterValue",true)
-- print(connect_to_topics)
-- rtt.provides("ros"):provides("rosparam"):setParameter("connect_to_topics", connect_to_topics)

-- ----------------- --
-- Create components --
-- ----------------- --
rtt.provides("ros"):create_named_node(ros_application_name)
dep:loadComponent(ros_application_name,"orocos_examples::SimpleRos")
simple_ros = dep:getPeer(ros_application_name)
simple_ros:addPeer(dep)
dep:setActivity(ros_application_name,app_period,1,1)
simple_ros:loadService("rosparam")
simple_ros:loadService("rosservice")

ctt_prop=simple_ros:getProperty("connect_to_topics")
ctt_prop:set(connect_to_topics)

-- -------------------- --
-- Configure components --
-- -------------------- --
simple_ros:configure()

-- -- ------------------ --
-- -- Create connections --
-- -- ------------------ --
-- cp = rtt.Variable('ConnPolicy')
-- cp.type = 0 -- type DATA
-- cp_ros = rtt.Variable('ConnPolicy')
-- cp_ros.transport = 3   -- transport layer: ROS

-- -- set the OROCOS port connections ################ replace by the name
-- dep:connect( virt_force:getName()..".out_joint_torques", virt_iface:getName()..".in_joint_torques", cp )

-- ---------------- --
-- Start components --
-- ---------------- --
simple_ros:start()

print("-----------------------------------------------------------------")