/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#ifndef OROCOS_EXAMPLES_SIMPLE_ROS_HPP
#define OROCOS_EXAMPLES_SIMPLE_ROS_HPP

#include <rtt/Component.hpp>
#include <rtt/RTT.hpp>
#include <rtt_ros2_services/rosservice.hpp>
#include <rtt_ros2_topics/rostopic.hpp>
#include <rtt_ros2_params/rosparam.hpp>
#include <std_msgs/typekit/Types.hpp>
#include <oe_msgs/srv/operation.hpp>

namespace orocos_examples
{
class SimpleRos : public RTT::TaskContext
{
  public:
    explicit SimpleRos(const std::string& name);
    virtual ~SimpleRos();

    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    bool connect_to_topics_;

    int32_t property_;
    int32_t input_port_value_;
    int32_t event_port_value_;
    int32_t event_port_cb_value_;
    int32_t op_callback_value_;

    // These ports are `int` ports, and will automatically map to `std_msgs/Int32` when connected to a ROS topic
    RTT::InputPort<std_msgs::msg::Int32> input_port_;
    RTT::OutputPort<std_msgs::msg::Int32> output_port_;

    // These ports are `std_msgs::Int32` to show that you can use ROS messages, too.
    RTT::InputPort<std_msgs::msg::Int32> event_port_;
    RTT::InputPort<std_msgs::msg::Int32> event_port_cb_;
    void eventPortCallback();

    // Operations and OperationCallers that are tied to ROS services must share the same callback function definition as ROS services, though
    // (but with non-const reference arguments, no shared pointers).
    RTT::OperationCaller<void(oe_msgs::srv::Operation::Request&, oe_msgs::srv::Operation::Response&)> op_caller_;
    void op_callback(oe_msgs::srv::Operation::Request&, oe_msgs::srv::Operation::Response&);
};
}  // namespace orocos_examples

#endif
