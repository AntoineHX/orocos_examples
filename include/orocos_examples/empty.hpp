/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#ifndef OROCOS_EXAMPLES_EMPTY_HPP
#define OROCOS_EXAMPLES_EMPTY_HPP

#include <rtt/Component.hpp>
#include <rtt/RTT.hpp>

namespace orocos_examples
{
class Empty : public RTT::TaskContext
{
  public:
    explicit Empty(const std::string& name);
    virtual ~Empty();
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
    void errorHook();
};
}  // namespace orocos_examples

#endif
