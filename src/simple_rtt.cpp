/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <rtt/os/main.h>
#include <ocl/DeploymentComponent.hpp>

bool global_fn(int input)
{
    std::cout << "global_fn called successfully with argument = " << input << std::endl;
    return true;
}

int ORO_main(int argc, char* argv[])
{
    std::string component_name = "foo";
    OCL::DeploymentComponent deployer("Deployer");

    //
    // Step 1 : Instantiate Component
    //
    if (not deployer.import("orocos_examples"))
    {
        std::cerr << "Orocos package orocos_examples could not be imported." << std::endl;
        return 1;
    }

    if (not deployer.loadComponent(component_name, "orocos_examples::Simple"))
    {
        std::cerr << "Failed to load orocos_examples::Simple component named [foo]." << std::endl;
        return 1;
    }

    RTT::TaskContext* simple_component = deployer.getPeer(component_name);

    if (not simple_component)
    {
        std::cerr << "Failed to get pointer to [foo]." << std::endl;
        return 1;
    }

    //
    // Step 2 : Connect to Component Things
    //
    RTT::Property<int> prop_property = simple_component->getProperty("property");
    RTT::Property<int> prop_input_port_value = simple_component->getProperty("input_port_value");
    RTT::Property<int> prop_event_port_value = simple_component->getProperty("event_port_value");
    RTT::Property<int> prop_event_port_cb_value = simple_component->getProperty("event_port_cb_value");

    RTT::ConnPolicy input_port_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_input_port;
    connect_to_input_port.connectTo(simple_component->ports()->getPort("input_port"), input_port_policy);

    int output_port_value = 0;
    RTT::ConnPolicy output_port_policy = RTT::ConnPolicy::data();
    RTT::InputPort<int> connect_to_output_port;
    simple_component->ports()->getPort("output_port")->connectTo(&connect_to_output_port, output_port_policy);

    RTT::ConnPolicy event_port_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_event_port;
    connect_to_event_port.connectTo(simple_component->ports()->getPort("event_port"), event_port_policy);

    RTT::ConnPolicy event_port_cb_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_event_port_cb;
    connect_to_event_port_cb.connectTo(simple_component->ports()->getPort("event_port_cb"), event_port_cb_policy);

    RTT::OperationCaller<bool(int)> op_simple_operation;
    op_simple_operation = simple_component->provides("simple_service")->getOperation("simple_operation");

    deployer.addOperation("global", global_fn, RTT::ClientThread);
    deployer.connectOperations("foo.simple_operation_caller", "Deployer.global");

    //
    // Step 3 : Start Component
    //
    if (not simple_component->configure())
    {
        std::cerr << "Failed to configure [foo]." << std::endl;
        return 1;
    }

    if (not simple_component->start())
    {
        std::cerr << "Failed to start [foo]." << std::endl;
        return 1;
    }
    sleep(1);

    //
    // Step 4 : Interact with Component
    //
    std::cout << std::endl;
    std::cout << "##### Demonstration beginning now." << std::endl << std::endl;

    std::cout << ">> First, status quo." << std::endl;
    std::cout << ">> [ property | input_port_value | event_port_value | event_port_cb_value | output_port_value ]" << std::endl;
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Setting [property] to [1]." << std::endl;
    prop_property.set(1);
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Writing [2] to [input_port]. This won't change [input_port_value] until after `update` is called." << std::endl;
    connect_to_input_port.write(2);
    sleep(1);  // so ports can exchange data, it doesn't actually take this long, but it's not instaneous
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Writing [3] to [event_port]. This will change [event_port_value] because `update` is automatically called." << std::endl;
    std::cout << ">> The `update` call will also call [simple_operation_caller] which will call [global_fn]." << std::endl;
    connect_to_event_port.write(3);
    sleep(1);  // so ports can exchange data, it doesn't actually take this long, but it's not instaneous
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Writing [4] to [event_port_cb]. This will change [event_port_cb_value] but will not change [output_port_value] because `update` isn't called."
              << std::endl;
    connect_to_event_port_cb.write(4);
    sleep(1);  // so ports can exchange data, it doesn't actually take this long, but it's not instaneous
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Calling [simple_operation] with value [5]." << std::endl;
    op_simple_operation(5);
    sleep(1);  // so ports can exchange data, it doesn't actually take this long, but it's not instaneous
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << ">> Finally setting the component to run periodically with a period of [1] second, and will run for [3] seconds." << std::endl;
    simple_component->setPeriod(1);
    sleep(3.1);  // so component thread can run
    simple_component->setPeriod(0);
    sleep(1);  // so ports can exchange data, it doesn't actually take this long, but it's not instaneous
    connect_to_output_port.read(output_port_value);
    std::cout << "[ " << prop_property.get() << " | " << prop_input_port_value.get() << " | " << prop_event_port_value.get() << " | " << prop_event_port_cb_value.get()
              << " | " << output_port_value << " ]" << std::endl;
    std::cout << std::endl;

    std::cout << "##### Demonstration complete." << std::endl << std::endl;

    //
    // Step 5 : Say Goodbye
    //
    if (not simple_component->stop())
    {
        std::cerr << "Failed to stop [foo]." << std::endl;
        return 1;
    }

    if (not simple_component->cleanup())
    {
        std::cerr << "Failed to cleanup [foo]." << std::endl;
        return 1;
    }

    if (not deployer.kickOutAll())
    {
        std::cerr << "Failed to unload [foo]." << std::endl;
        return 1;
    }

    deployer.shutdownDeployment();

    return 0;
}
