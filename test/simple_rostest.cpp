/*
 * Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <gtest/gtest.h>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/int32.hpp>
#include <oe_msgs/srv/operation.hpp>

constexpr std::size_t kNumberOfThreads = 2;
static rclcpp::executor::Executor::SharedPtr node_executor;
static rclcpp::Node::SharedPtr local_node;
class std::shared_ptr<rclcpp::SyncParametersClient> parameters_client;

std_msgs::msg::Int32 output_port_msg;
void mock_outputCallback(std_msgs::msg::Int32::ConstSharedPtr msg)
{
    output_port_msg.data = msg->data;
}

void mock_fn(oe_msgs::srv::Operation::Request::SharedPtr request, oe_msgs::srv::Operation::Response::SharedPtr response)
{
    (void)request;
    response->value.data = true;
}

TEST(SimpleRosTest, EverythingBecauseRosTestDoesntHelpWithRealTesting)
{
    ASSERT_TRUE(rclcpp::ok());
    ASSERT_TRUE(local_node);

    bool simple_found = false;
    int retry_attempts = 0;

    while ((not simple_found) and (retry_attempts < 5))
    {
        std::vector<std::string> nodes;
        nodes = local_node->get_node_names();

        for (auto const& node_it : nodes)
        {
            if (node_it.compare("/simple") == 0)
            {
                simple_found = true;
            }
        }

        if (not simple_found)
        {
            std::cout << "Waiting for node under test..." << std::endl;
            retry_attempts += 1;
            sleep(1);
        }
    }

    ASSERT_TRUE(simple_found);

    // Create some test interaction objects

    std_msgs::msg::Int32 input_port_msg, event_port_msg, event_port_cb_msg;
    auto connect_to_input_port = local_node->create_publisher<std_msgs::msg::Int32>("input_port", 1000);
    auto connect_to_event_port = local_node->create_publisher<std_msgs::msg::Int32>("event_port", 1000);
    auto connect_to_event_port_cb = local_node->create_publisher<std_msgs::msg::Int32>("event_port_cb", 1000);
    auto connect_to_output_port = local_node->create_subscription<std_msgs::msg::Int32>("output_port", 1000, mock_outputCallback);

    struct
    {
        oe_msgs::srv::Operation::Request::SharedPtr request = std::make_shared<oe_msgs::srv::Operation::Request>();
        oe_msgs::srv::Operation::Response::SharedPtr response;
    } operation_msg;
    auto srv_simple_operation = local_node->create_client<oe_msgs::srv::Operation>("/simple/simple_operation");

    bool operation_found = false;
    retry_attempts = 0;

    while ((not operation_found) and (retry_attempts < 5))
    {
        if (srv_simple_operation->wait_for_service(std::chrono::seconds(1)))
        {
            operation_found = true;
        }

        if (not operation_found)
        {
            std::cout << "Waiting for operation under test..." << std::endl;
            retry_attempts += 1;
            sleep(1);
        }
    }

    ASSERT_TRUE(operation_found);
    EXPECT_TRUE(srv_simple_operation->service_is_ready());

    auto srv_mock = local_node->create_service<oe_msgs::srv::Operation>("/external/operation", mock_fn);

    bool parameter_client_connected = false;
    retry_attempts = 0;

    while ((not parameter_client_connected) and (retry_attempts < 5))
    {
        if (parameters_client->wait_for_service(std::chrono::seconds(1)))
        {
            parameter_client_connected = true;
        }

        if (not parameter_client_connected)
        {
            std::cout << "Waiting for operation under test..." << std::endl;
            retry_attempts += 1;
            sleep(1);
        }
    }

    ASSERT_TRUE(parameter_client_connected);
    EXPECT_TRUE(parameters_client->service_is_ready());

    int property, input_port_value, event_port_value, event_port_cb_value;
    ASSERT_TRUE(parameters_client->has_parameter("property"));
    ASSERT_TRUE(parameters_client->has_parameter("input_port_value"));
    ASSERT_TRUE(parameters_client->has_parameter("event_port_value"));
    ASSERT_TRUE(parameters_client->has_parameter("event_port_cb_value"));

    ASSERT_EQ(0, parameters_client->get_parameter("property", -999));
    ASSERT_EQ(0, parameters_client->get_parameter("input_port_value", -999));
    ASSERT_EQ(0, parameters_client->get_parameter("event_port_value", -999));
    ASSERT_EQ(0, parameters_client->get_parameter("event_port_cb_value", -999));

    // Run some actual tests.
    // SimpleRos components are not configured to allow remote setting of execution rate, so this test will not exactly mirror `simple_test`.

    sleep(1);
    EXPECT_EQ(0, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(0, output_port_msg.data);

    auto set_parameters_results = parameters_client->set_parameters({ rclcpp::Parameter("property", 1) });
    for (auto& result : set_parameters_results)
    {
        ASSERT_TRUE(result.successful);
    }

    sleep(1);
    EXPECT_EQ(1, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(1, output_port_msg.data);

    input_port_msg.data = 2;
    connect_to_input_port->publish(input_port_msg);
    sleep(1);
    EXPECT_EQ(1, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(2, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(3, output_port_msg.data);

    event_port_msg.data = 3;
    connect_to_event_port->publish(event_port_msg);
    sleep(1);
    EXPECT_EQ(1, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(2, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(3, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(0, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(6, output_port_msg.data);

    event_port_cb_msg.data = 4;
    connect_to_event_port_cb->publish(event_port_cb_msg);
    sleep(1);
    EXPECT_EQ(1, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(2, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(3, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(4, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(10, output_port_msg.data);

    operation_msg.request->value.data = 5;
    operation_msg.response = srv_simple_operation->async_send_request(operation_msg.request).get();
    ASSERT_TRUE(operation_msg.response);
    EXPECT_TRUE(operation_msg.response->value.data);
    sleep(1);
    EXPECT_EQ(1, parameters_client->get_parameter("property", -999));
    EXPECT_EQ(2, parameters_client->get_parameter("input_port_value", -999));
    EXPECT_EQ(3, parameters_client->get_parameter("event_port_value", -999));
    EXPECT_EQ(4, parameters_client->get_parameter("event_port_cb_value", -999));
    EXPECT_EQ(10, output_port_msg.data);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    rclcpp::init(argc, argv);
    node_executor = std::make_shared<rclcpp::executors::MultiThreadedExecutor>(rclcpp::executor::ExecutorArgs(), kNumberOfThreads);

    local_node = std::make_shared<rclcpp::Node>("simple_rostest");
    parameters_client = std::make_shared<rclcpp::SyncParametersClient>(local_node, "simple");
    // node_executor->add_node(local_node);

    auto spinner = std::thread([&]() {
        while (rclcpp::ok())
        {
            node_executor->spin_some();
        }
    });

    int ret = RUN_ALL_TESTS();

    node_executor->cancel();
    spinner.join();

    return ret;
}
